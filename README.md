# Κατανόηση /ka.taˈno.i.si/ – Understanding

Allows machine to understand human language.

Supported locales:
uk_UA
en_US
es_ES

# Key concepts

- Locale - a collection of parameters that is enough to consistently define a natural language. Typically consists of the language and the geographical location.

- Text - a sequence of symbols that is expected to have semantical representation in a specific locale
- Context - information about the world which is helpful for making correct decision
- Morpheme - a unit of lexical representation
- Sememe - a unit of semantical representation
- Grapheme
- Lemma
- Semantic
- Syntax

# Language expression

When we are talking about text understanding we do not really mean understanding of the text itself. What we want to achieve is to understand what is behind the text, what it creator inteded to express. In this terms it would be helpful to see the problem as a game, a game where the author has something on its mind end the interpreter is trying to guess what it is. The reassuring bit is that both the author and the interpreter want for interpreter to win so the prior gives hints.
We will call this thing that the author has on its mind a *message*. And the hints we will be using is text.
In this project we will attempt to play the role of interpreter and decode hints that the author has given. But these hints are not enough to fully decode the message, because message is always in the context. The message can not be the only purpose of commincation, this would be as pointless as a being that is born to only listen to the message and then die (but even in this case it would have more sense). And this context is vast and has many layer. When we say "What time is it?" the person on the other end knows which time zone I am talking about, and it has general understanding of how precise it needs to be when tellin me the time. But it does not stop there - the whole concept of the time is known to both, it is shared context, but it is so common that we do not even notice it. Like fish that does not know what water is.
But not only message exists in context, it is also expected to make changes. When we read a book we are receiving knowledge which will help shap our views and, consequently, our actions. When we ask a question we expect to receive the answer. When we command we expect a specific action in response. There is alwayss a reaction to any message.
Second challenge, which is probably a significantly more complex, is to create a complete and unambiguous representation of the semantics.
