# This doucment describes various data and its flow through the system

1. Raw text

This type of data has no explicit machine readable structure and only contains characters in a known encoding. For example:

```txt
a smudge
```

2. Marked text

This text with additional information regarding locale used. It can be encoded in XLM format in the following way:

```xml
<t xml:lang="en">a smudge</t>
```

Mared text can have a more complex structure:

```xml
<t xml:lang="en">As it was said "<t xml:lang="fr">à la guerre comme à la guerre</t>" which means "at war as at war".</t>
```

```xml
<t xml:lang="la">Quis custodiet ipsos custodes?</t><t xml:lang="en">How power can be held to account?</t>
```

Note: XML representation is meant to only be used for being read by humans and not for communication between different computer systems.


